import Image from "next/image";
import { FC, ReactNode } from "react";

interface QuestionProps {
  children: ReactNode;
}

const Question: FC<QuestionProps> = ({ children }) => {
  return (
    <blockquote className="border-l-4 border-blue-500 pl-4 mb-2 mt-8 text-gray-400">
      {children}
    </blockquote>
  );
};

interface AnswerProps {
  children: ReactNode;
}

const Answer: FC<AnswerProps> = ({ children }) => {
  return <div className="pb-2">{children}</div>;
};

const codeString = `
const nextConfig = {
  ...
  async redirects() {
    return [
      ...
      {
        source: "/blog",
        destination: "https://example.com",
        permanent: true,
      },
    ];
  },
};`;

export default function Home() {
  return (
    <main className="p-8">
      <h1 className="text-2xl font-bold mb-4">Exercise</h1>
      <Question>
        1. From{" "}
        <a
          className="underline"
          href="https://gist.github.com/Pieparker/b04a4e9ff82ba949e5db9d5b0e9d89e8"
        >
          this list
        </a>
        , rank your 5 most favourite and 5 least favourite support tasks.
        Provide a brief explanation for each.
      </Question>
      <Answer>
        <h2 className="text-xl font-bold">Favourite:</h2>
        <ul className="list-disc pl-4">
          <li>
            Dig through logs to troubleshoot a customer&apos;s broken project
          </li>
          <p>
            Explanation: I enjoy the process of investigating and identifying
            the root cause of issues.
          </p>
          <li>Write and maintain support articles and docs pages</li>
          <p>
            Explanation: I like creating resources that help others and improve
            their experience.
          </p>
          <li>
            Analyze hundreds of support tickets to spot trends the product team
            can use
          </li>
          <p>
            Explanation: Spotting patterns and providing insights to improve the
            product is rewarding.
          </p>
          <li>
            Identify, file (and, where possible, resolve) bugs in private and
            public Vercel/Next.js repos on GitHub
          </li>
          <p>
            Explanation: Contributing to open-source projects and improving the
            codebase is fulfilling.
          </p>
          <li>Help train and onboard new support teammates</li>
          <p>
            Explanation: Sharing knowledge and ensuring new teammates are
            well-prepared is gratifying.
          </p>
        </ul>
        <h2 className="text-xl font-bold">Least favourite:</h2>
        <ul className="list-disc pl-4">
          <li>Help resolve billing issues for customers</li>
          <p>
            Explanation: Billing issues can be repetitive and less technically
            challenging.
          </p>
          <li>
            Scheduling time-off coverage and collaborating as part of a growing
            cohesive support team
          </li>
          <p>
            Explanation: Scheduling can be logistical and less engaging than
            technical tasks.
          </p>
          <li>Manage a support team</li>
          <p>
            Explanation: Managing a team requires significant administrative
            work, which can be less engaging than technical problem-solving.
          </p>
          <li>Find and recruit teammates for the support team</li>
          <p>
            Explanation: Recruiting can be time-consuming and less aligned with
            my technical skills.
          </p>
          <li>
            Respond to queries on Twitter, Reddit, Hacker News, and other 3rd
            party sites
          </li>
          <p>
            Explanation: Social media support can be unpredictable and often
            lacks the depth of direct customer interactions.
          </p>
        </ul>
      </Answer>
      <Question>2. What do you want to learn or do more of at work?</Question>
      <Answer>
        While I have experience with React and deploying containers, I want to
        learn more about modern React practices, how Vercel deploys and hosts
        projects, and better understand how edge and serverless functions work
        under the hood. Additionally, I want to learn how Vercel handles these
        deployments, the common pitfalls encountered, the types of problems that
        usually arise, and the solutions implemented to resolve these issues.
        Understanding these aspects will enhance my ability to support and
        optimize deployment processes effectively.
      </Answer>
      <Question>
        3. Describe how you solved a challenge or technical issue that you faced
        in a previous role (preferably in a previous support role). How did you
        determine that your solution was successful?
      </Question>
      <Answer>
        <ul className="list-disc pl-4">
          <li>
            <strong>Collect diagnostics:</strong> Gather symptoms (expected and
            observed behavior), logs, and metrics.
          </li>
          <li>
            <strong>Check known issues:</strong> Use automation tooling and the
            knowledge base to see if it’s a known problem.
          </li>
          <li>
            <strong>Manual analysis:</strong> If the issue is unknown, manually
            analyze the diagnostic data to identify obvious issues.
          </li>
          <li>
            <strong>Reproduce the issue:</strong> Try to reproduce the issue in
            an environment similar to the customer’s.
          </li>
          <li>
            <strong>Report and workaround:</strong> Report a bug to the
            engineering team and find a workaround if possible. If the issue is
            due to unsupported actions, guide the customer to a supported
            solution.
          </li>
          <li>
            <strong>Customer communication:</strong> Briefly describe the
            investigation and provide the solution, workaround, and other
            findings to the customer.
          </li>
          <li>
            <strong>Update knowledge base:</strong> Update the tooling and
            knowledge base with new information if necessary.
          </li>
          <li>
            To determine that the solution was successful I ask the customer if
            the recommendation was helpful and if they need any further help
          </li>
        </ul>
      </Answer>
      <Question>
        4. When would you choose to use Edge Functions, Serverless Functions, or
        Edge Middleware with Vercel?
      </Question>
      <Answer>
        <h2 className="text-xl font-bold">Edge Functions:</h2>
        <ul className="list-disc pl-4">
          <li>
            Edge functions run on servers that are as close to the end user (or
            a database) as possible to achieve low latency.
          </li>
          <li>
            Some Node.js APIs and JavaScript language features are not
            available.
          </li>
          <li>
            Use for handling relatively simple dynamic server-side logic with
            low latency: dynamic content generation, fetching data from external
            APIs, form submission handling, etc.
          </li>
        </ul>
        <h2 className="text-xl font-bold">Serverless Functions:</h2>
        <ul className="list-disc pl-4">
          <li>
            Serverless function reside at regional data centers and provide
            higher latency in comparison to edge functions and middleware.
          </li>
          <li>Full support for Node.js and JavaScript language features</li>
          <li>
            Use for streaming data, authentication and authorization, intensive
            data processing tasks.
          </li>
        </ul>
        <h2 className="text-xl font-bold">Edge Middleware:</h2>
        <ul className="list-disc pl-4">
          <li>
            Edge middleware uses the same edge engine as edge functions and
            located at the same data centers (low latency). They run before the
            edge network cache.
          </li>
          <li>Limited Node.js support plus Edge Middleware API</li>
          <li>
            Use for redirects, rewrites, header modification, A/B testing,
            feature flags, etc.
          </li>
        </ul>
      </Answer>
      <Question>
        5. Imagine a customer writes in requesting help with a build issue on a
        framework or technology that you&apos;ve not seen before. How would you
        begin troubleshooting this and what questions would you ask the customer
        to understand the situation better?
      </Question>
      <Answer>
        I would request the following information:
        <ul className="list-disc pl-4">
          <li>Expected and observed behavior (if not obvious)</li>
          <li>Steps the customer performed</li>
          <li>Name and version of the framework or technology</li>
          <li>
            Did the build work before? What changes were made in the build
            environment?
          </li>
          <li>
            Which documentation or guide did they use to organize the build?
          </li>
          <li>Logs and metrics</li>
          <li>
            Information about the environment: platform, disk space, runtime
            version, etc.
          </li>
        </ul>
        I would perform the following steps:
        <ul className="list-decimal pl-6">
          <li>
            Determine if the framework or technology is supported on our
            platform.
          </li>
          <li>Determine if the customer&apos;s environment is supported.</li>
          <li>Check if the customer followed the documentation correctly</li>
          <li>
            Try to reproduce the issue as close as possible to the
            customer&apos;s environment
          </li>
          <li>For further steps, see the answer for the question 3.</li>
        </ul>
      </Answer>
      <Question>
        6. The customer from question 5 replies to your response with the below:
        <br />
        “I&apos;m so frustrated. I&apos;ve been trying to make this work for
        hours and I just can&apos;t figure it out. It must be a platform issue
        so just fix it for me instead of asking me questions.”
        <br />
        Please write a follow-up reply to the customer.
      </Question>
      <Answer>
        Dear [customer name],
        <br />
        <br />I understand how frustrating it can be when you&apos;re trying to
        make something work and it just won&apos;t cooperate. I&apos;ll do my
        best to help you with this issue. The information I requested is
        necessary to understand the situation better and provide you with the
        most accurate solution. I&apos;ll do my best to make this process as
        smooth as possible for you.
        <br />
        [If there is a quick workaround that could help the customer to move
        forward, I would provide it here.]
        <br />
        These questions are tailored to help me understand the issue better and
        provide you with the best possible solution. I appreciate your
        understanding and patience. I look forward to your response.
        <br />
        <br />
        Thank you,
        <br />
        =Andrey
      </Answer>
      <Question>
        7. A customer writes in to the Helpdesk asking &apos;How do I do a
        redirect from the /blog path to https://example.com?&apos; Please write
        a reply to the customer. Feel free to add any information about your
        decision making process after the reply.
      </Question>
      <Answer>
        Let&apos;s assume that the customer is using the Next.js framework. My
        answer would look like this:
        <br />
        <br />
        Dear [customer name],
        <br />
        <br />
        Thank you for contacting the Vercel support team. To redirect from the{" "}
        <code>/blog</code> path to <code>https://example.com</code>, you can
        modify the <code>next.config.mjs</code> file by adding the following
        lines:
        <pre>
          <code>{codeString}</code>
        </pre>
        If you plan to change the destination URL in the future or remove the
        redirect, you can set the <code>permanent</code> flag to{" "}
        <code>false</code>. You can find more information about redirects{" "}
        <a
          className="underline"
          href="https://nextjs.org/docs/app/building-your-application/routing/redirecting#redirects-in-nextconfigjs"
        >
          here
        </a>
        . Please let me know if you have further questions.
        <br />
        <br />
        Thank you,
        <br />
        =Andrey
      </Answer>
      <Question>
        8. A customer is creating a site and would like their project not to be
        indexed by search engines. Please write a reply to the customer. Feel
        free to add any information about your decision making process after the
        reply.
      </Question>
      <Answer>
        Dear [customer name],
        <br />
        <br />
        Thank you for contacting the Vercel support team. To prevent your
        project from being indexed by search engines you can add a{" "}
        <code>robots.txt</code> file to the <code>app</code> directory of your
        project. The file should contain the following lines:
        <pre>
          <code>{`User-agent: *
Disallow: /`}</code>
        </pre>
        Please note that if the site was already indexed by search engines, it
        may take some time for the changes to take effect. You can find more
        information about the <code>robots.txt</code> file{" "}
        <a
          className="underline"
          href="https://nextjs.org/docs/app/api-reference/file-conventions/metadata/robots"
        >
          here
        </a>
        .
        <br />
        <br />
        Thank you,
        <br />
        =Andrey
      </Answer>
      <Question>
        9. What do you think is one of the most common problems which customers
        ask Vercel for help with? How would you help customers to overcome
        common problems, short-term and long-term?
      </Question>
      <Answer>
        I believe that one of the most common problems customers ask Vercel for
        help with is deployment issues. To help customers overcome common
        problems in the short-term, I would create a dedicated knowledge base
        article or documentation page with the most common deployment issues and
        solutions. I would add a link to this article in the error message that
        the customer sees when the deployment fails. I would make sure that the
        article contains real error messages to improve searchability.
        <br />
        <br />
        Long term, I would work with the engineering team to improve the
        deployment process and error messages to make them more user-friendly
        and informative. One of the ways to achieve that is to leverage{" "}
        <a
          className="underline"
          href="https://en.wikipedia.org/wiki/Prompt_engineering#Retrieval-augmented_generation"
        >
          Retrieval-augmented generation
        </a>
        .
        <br />
        <br />I would also work with the product team to improve the deployment
        dashboard so that it displays bespoke automatically generated solutions.
        Another way to decrease the amount of deployment issues is to check a
        project for as much common issues as possible in the development
        environment before the deployment starts.
      </Answer>
      <Question>
        10. How could we improve or alter this familiarisation exercise?
      </Question>
      <Answer>
        I would add more details to some questions. For example, in the question
        5 I would add a specific technology or framework that the customer is
        asking about.
        <br />
        <br />
        The initial email wasn&apos;t very clear to me, so I wrote the following
        email to the recruiting coordinator:
        <br />
        1. Am I correct that I should track the time by myself?
        <br />
        2. I cannot start the task immediately, can I start tomorrow?
        <br />
        3. You mentioned that I should reply to this email thread when the task
        is done, but at the end of your email, there is a link &quot;submit
        here&quot;. What should I submit?
        <br />
        The recruiting coordinator replied, &quot;Thank you for pointing out
        this discrepancy. I&apos;ve updated the email moving forward!&quot;
      </Answer>
      <p>Thank you!</p>
    </main>
  );
}
