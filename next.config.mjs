/** @type {import('next').NextConfig} */
const nextConfig = {
  async redirects() {
    return [
      {
        source: "/blog",
        destination: "https://example.com",
        permanent: true,
      },
    ];
  },
};

export default nextConfig;
